package http

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func SetUpRouter(server *echo.Echo, handler *Handler) {
	server.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Service Is Running")
	})
}

