package server

import (
	"dunyu/internal/interfaces/container"
	"dunyu/internal/interfaces/server/http"
)

func StartService(container *container.Container) {
	http.StartHttpService(container)
}

