package config

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
)

type DefaultConfig struct {
	// TODO: add json tag ex "json:"name""
	Apps struct {
		Name     string 
		HttpPort string 
		GrpcPort string 
	}
}

func New(path string) *DefaultConfig {
	fmt.Sprintf("Try new config...")

	viper.SetConfigFile(path)
	viper.SetConfigType("json")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
	defaultConfig := DefaultConfig{}
	err := viper.Unmarshal(&defaultConfig)
	if err != nil {
		panic(err)
	}

	b, _ := json.Marshal(defaultConfig)
	fmt.Println(b)

	return &defaultConfig
}

func (c *DefaultConfig) AppAddress() string {
	return fmt.Sprintf(":%v", c.Apps.HttpPort)
}

