package http

import (
	"dunyu/internal/interfaces/container"
	"github.com/labstack/echo/v4"
)

func StartHttpService(container *container.Container) {
	server := echo.New()

	SetUpRouter(server, SetUpHandler(container))

	if err := server.Start(container.Config.AppAddress()); err != nil {
		panic(err)
	}
}

