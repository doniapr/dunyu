package container

import (
	Config "dunyu/internal/shared/config"
	"fmt"
)

type Container struct {
	Config *Config.DefaultConfig
}

func New() *Container {
	fmt.Println("Try NewContainer ... ")

	// ========Construct Config
	config := Config.New("./resources/config.json")
	container := &Container{
		Config: config,
	}

	return container
}

