package main

import (
	Container "dunyu/internal/interfaces/container"
	. "dunyu/internal/interfaces/server"
)

func main() {
	StartService(Container.New())
}
